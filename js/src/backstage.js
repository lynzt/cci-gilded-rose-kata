const updateBackstage = item => {
  item.sell_in--;
  item.quality++;

  if (item.sell_in >= 5 && item.sell_in < 10) item.quality++;
  if (item.sell_in >= 0 && item.sell_in < 5) item.quality+=2;
  if (item.sell_in < 0) item.quality = 0;
  item.quality = ensureValidQualityRange(item.quality);
}

const itemNameTransform = {
  'Aged Brie': updateBrie,
  'Sulfuras, Hand of Ragnaros': updateSulfuras,
  'Backstage passes to a TAFKAL80ETC concert': updateBackstage,
  'Conjured Mana Cake': updateConjured,
  'Normal': updateNormal
};

const updateConjured = item => {
  item.sell_in--;
  item.quality-=2;

  if (item.sell_in < 0) item.quality-=2;
  item.quality = ensureValidQualityRange(item.quality);
}

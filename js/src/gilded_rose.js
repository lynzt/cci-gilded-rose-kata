function Item(name, sell_in, quality) {
  this.name = name;
  this.sell_in = sell_in;
  this.quality = quality;
}

const itemUpdate = item => (itemNameTransform[item.name] || itemNameTransform['Normal'])(item);
const update_quality = items => items.map(item => itemUpdate(item));

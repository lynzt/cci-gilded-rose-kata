const updateBrie = item => {
  item.sell_in--;
  item.quality++;

  if (item.sell_in < 0) item.quality++;
  item.quality = ensureValidQualityRange(item.quality);
}

describe("Multiple Items", function() {
  it("Should sell_in should not change, and quality should not change", function() {
    const items = [];
    items.push(new Item("Sulfuras, Hand of Ragnaros", 1, 80))
    items.push(new Item("Normal", 1, 5))
    items.push(new Item("Aged Brie", -1, 49))
    items.push(new Item("Backstage passes to a TAFKAL80ETC concert", 10, 5))
    update_quality(items);
    expect(items[0]).toEqual({name: 'Sulfuras, Hand of Ragnaros', sell_in: 1, quality: 80});
    expect(items[1]).toEqual({name: 'Normal', sell_in: 0, quality: 4});
    expect(items[2]).toEqual({name: 'Aged Brie', sell_in: -2, quality: 50});
    expect(items[3]).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 9, quality: 7});
  });
});

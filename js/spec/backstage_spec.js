describe("Backstage passes to a TAFKAL80ETC concert Item", function() {
  describe("sell_in more than 10 days", function() {
    it("Should subtract 1 from sell_in, and add 1 to quality", function() {
      const item = new Item("Backstage passes to a TAFKAL80ETC concert", 11, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 10, quality: 6});
    });
    describe("with quality 50", function() {
      it("Should subtract 1 from sell_in, and quality should remain 50", function() {
        const item = new Item("Backstage passes to a TAFKAL80ETC concert", 11, 50);
        update_quality([item]);
        expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 10, quality: 50});
      });
    });
  });

  describe("sell_in 6 to 10 days", function() {
    describe("sell_in 10 days", function() {
      it("Should subtract 1 from sell_in, and add 2 from quality", function() {
        const item = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 5);
        update_quality([item]);
        expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 9, quality: 7});
      });
      describe("with quality 49", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 49);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 9, quality: 50});
        });
      });
      describe("with quality 50", function() {
        it("Should subtract 1 from sell_in, and quality should remain 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 10, 50);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 9, quality: 50});
        });
      });
    });
    describe("sell_in 6 days", function() {
      it("Should subtract 1 from sell_in, and add 2 from quality", function() {
        const item = new Item("Backstage passes to a TAFKAL80ETC concert", 6, 5);
        update_quality([item]);
        expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 5, quality: 7});
      });
      describe("with quality 49", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 6, 49);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 5, quality: 50});
        });
      });
      describe("with quality 50", function() {
        it("Should subtract 1 from sell_in, and quality should remain 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 6, 50);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 5, quality: 50});
        });
      });
    });
  });

  describe("sell_in 1 to 5 days", function() {
    describe("sell_in 5 days", function() {
      it("Should subtract 1 from sell_in, and add 3 from quality", function() {
        const item = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 5);
        update_quality([item]);
        expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 4, quality: 8});
      });
      describe("with quality 48", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 48);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 4, quality: 50});
        });
      });
      describe("with quality 49", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 49);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 4, quality: 50});
        });
      });
      describe("with quality 50", function() {
        it("Should subtract 1 from sell_in, and quality should remain 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 5, 50);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 4, quality: 50});
        });
      });
    });

    describe("sell_in 1 day", function() {
      it("Should subtract 1 from sell_in, and add 3 from quality", function() {
        const item = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 5);
        update_quality([item]);
        expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 0, quality: 8});
      });
      describe("with quality 48", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 48);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 0, quality: 50});
        });
      });
      describe("with quality 49", function() {
        it("Should subtract 1 from sell_in, and quality should max to 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 49);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 0, quality: 50});
        });
      });
      describe("with quality 50", function() {
        it("Should subtract 1 from sell_in, and quality should remain 50", function() {
          const item = new Item("Backstage passes to a TAFKAL80ETC concert", 1, 50);
          update_quality([item]);
          expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: 0, quality: 50});
        });
      });
    });
  });

  describe("sell_in passed", function() {
    it("Should subtract 1 from sell_in, and quality should be 0", function() {
      const item = new Item("Backstage passes to a TAFKAL80ETC concert", 0, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: -1, quality: 0});
    });
    it("Should subtract 1 from sell_in, and quality should be 0", function() {
      const item = new Item("Backstage passes to a TAFKAL80ETC concert", -1, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Backstage passes to a TAFKAL80ETC concert', sell_in: -2, quality: 0});
    });
  });


});

describe("Conjured Mana Cake Item", function() {
  describe("sell_in before", function() {
    it("Should subtract 1 from sell_in, and subtract 2 from quality", function() {
      const item = new Item("Conjured Mana Cake", 1, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: 0, quality: 3});
    });
    describe("with quality 1", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", 1, 0);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: 0, quality: 0});
      });
    });
    describe("with quality 0", function() {
      it("Should subtract 1 from sell_in, and quality should remain 0", function() {
        const item = new Item("Conjured Mana Cake", 1, 0);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: 0, quality: 0});
      });
    });
  });

  describe("sell_in on", function() {
    it("Should subtract 1 from sell_in, and subtract 4 from quality", function() {
      const item = new Item("Conjured Mana Cake", 0, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -1, quality: 1});
    });
    describe("with quality 3", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", 0, 3);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -1, quality: 0});
      });
    });
    describe("with quality 2", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", 0, 2);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -1, quality: 0});
      });
    });
    describe("with quality 1", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", 0, 1);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -1, quality: 0});
      });
    });
    describe("with quality 0", function() {
      it("Should subtract 1 from sell_in, and quality remain 0", function() {
        const item = new Item("Conjured Mana Cake", 0, 0);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -1, quality: 0});
      });
    });
  });

  // redundant to sell on ??
  describe("sell_in after", function() {
    it("Should subtract 1 from sell_in, and subtract 4 from quality", function() {
      const item = new Item("Conjured Mana Cake", -1, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -2, quality: 1});
    });
    describe("with quality 3", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", -1, 3);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -2, quality: 0});
      });
    });
    describe("with quality 2", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", -1, 2);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -2, quality: 0});
      });
    });
    describe("with quality 1", function() {
      it("Should subtract 1 from sell_in, and quality min to 0", function() {
        const item = new Item("Conjured Mana Cake", -1, 1);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -2, quality: 0});
      });
    });
    describe("with quality 0", function() {
      it("Should subtract 1 from sell_in, and quality remain 0", function() {
        const item = new Item("Conjured Mana Cake", -1, 0);
        update_quality([item]);
        expect(item).toEqual({name: 'Conjured Mana Cake', sell_in: -2, quality: 0});
      });
    });
  });


});

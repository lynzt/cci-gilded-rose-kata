describe("Aged Brie Item", function() {
  describe("sell_in before", function() {
    it("Should subtract 1 from sell_in, and add 1 to quality", function() {
      const item = new Item("Aged Brie", 1, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Aged Brie', sell_in: 0, quality: 6});
    });
    describe("with quality 50", function() {
      it("Should subtract 1 from sell_in, and quality should remain 50", function() {
        const item = new Item("Aged Brie", 1, 50);
        update_quality([item]);
        expect(item).toEqual({name: 'Aged Brie', sell_in: 0, quality: 50});
      });
    });
  });

  describe("sell_in on", function() {
    it("Should subtract 1 from sell_in, and add 2 from quality", function() {
      const item = new Item("Aged Brie", 0, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Aged Brie', sell_in: -1, quality: 7});
    });
    describe("with quality 49", function() {
      it("Should subtract 1 from sell_in, and quality max to 50", function() {
        const item = new Item("Aged Brie", 0, 49);
        update_quality([item]);
        expect(item).toEqual({name: 'Aged Brie', sell_in: -1, quality: 50});
      });
    });
    describe("with quality 0", function() {
      it("Should subtract 1 from sell_in, and quality should remain 50", function() {
        const item = new Item("Aged Brie", 0, 50);
        update_quality([item]);
        expect(item).toEqual({name: 'Aged Brie', sell_in: -1, quality: 50});
      });
    });
  });

  describe("sell_in after", function() {
    it("Should subtract 1 from sell_in, and add 2 to quality", function() {
      const item = new Item("Aged Brie", -1, 5);
      update_quality([item]);
      expect(item).toEqual({name: 'Aged Brie', sell_in: -2, quality: 7});
    });
    describe("with quality 49", function() {
      it("Should subtract 1 from sell_in, and quality max to 50", function() {
        const item = new Item("Aged Brie", -1, 49);
        update_quality([item]);
        expect(item).toEqual({name: 'Aged Brie', sell_in: -2, quality: 50});
      });
    });
    describe("with quality 50", function() {
      it("Should subtract 1 from sell_in, and quality should remain 50", function() {
        const item = new Item("Aged Brie", -1, 50);
        update_quality([item]);
        expect(item).toEqual({name: 'Aged Brie', sell_in: -2, quality: 50});
      });
    });
  });


});

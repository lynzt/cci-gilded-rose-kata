describe("Gilded Rose", function() {

  it("should foo", function() {
    items = [ new Item("foo", 0, 0) ];
    update_quality();
    expect(items[0].name).toEqual("fixme");
  });

  it("Should subtract 1 from sell_in, and subtract 1 from quality", function() {
    const item = new Item("Normal", 5, 5);
    update_quality([item]);
    expect(item).toEqual({name: 'Normal', sell_in: 4, quality: 4});
  });

  it("Should subtract 1 from sell_in, and subtract 2 from quality", function() {
    const item = new Item("Normal", -1, 5);
    update_quality([item]);
    expect(item).toEqual({name: 'Normal', sell_in: -2, quality: 3});
  });

});

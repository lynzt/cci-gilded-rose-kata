describe("Sulfuras, Hand of Ragnaros Item", function() {
  describe("sell_in before", function() {
    it("Should sell_in should not change, and quality should not change", function() {
      const item = new Item("Sulfuras, Hand of Ragnaros", 1, 80);
      update_quality([item]);
      expect(item).toEqual({name: 'Sulfuras, Hand of Ragnaros', sell_in: 1, quality: 80});
    });
  });

  describe("sell_in on", function() {
    it("Should sell_in should not change, and quality should not change", function() {
      const item = new Item("Sulfuras, Hand of Ragnaros", 0, 80);
      update_quality([item]);
      expect(item).toEqual({name: 'Sulfuras, Hand of Ragnaros', sell_in: 0, quality: 80});
    });
  });

  describe("sell_in after", function() {
    it("Should sell_in should not change, and quality should not change", function() {
      const item = new Item("Sulfuras, Hand of Ragnaros", -1, 80);
      update_quality([item]);
      expect(item).toEqual({name: 'Sulfuras, Hand of Ragnaros', sell_in: -1, quality: 80});
    });
  });

});
